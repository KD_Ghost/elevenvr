# elevenvr

# [查分网页](https://kd_ghost.gitee.io/elevenvr/) 
## **介绍**

此为 VR游戏 《致胜11分》 简化查分工具  
API 来源 为 [www.elevenvr.club](https://www.elevenvr.club) 官网 API  
index.html 主页为单用户查询

search_all_user.html 为多用户批量查询  
批量查询样例excel文件为[allUserGrade.xls文件](https://kd_ghost.gitee.io/elevenvr/allUserGrade.xls)
